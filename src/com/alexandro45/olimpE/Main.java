package com.alexandro45.olimpE;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    int y;
    int x;
    int width = 0;
    int height = 0;
    int xOut = 0;
    int yOut = 0;
    int [][] map;

    Main() {
        Scanner s = null;
        try {
            s = new Scanner(new FileInputStream("e.dat"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        y = s.nextInt() - 1;
        x = s.nextInt() - 1;
        yOut = s.nextInt();
        xOut = s.nextInt();
        height = s.nextInt();
        width = s.nextInt();

        map = new int[height][width];
        for (int y = 0;y < height;y++) {
            for (int x = 0;x < width;x++) {
                int i = Integer.parseInt(s.next());
                System.out.print(i + " ");
                map[y][x] = i;
            }
            System.out.print("\n");
        }
        int[] bkb = to(x,y);
        System.out.println((bkb[0]+1) + " " + (bkb[1]+1));//TODO захтыв спати

    }

    private int[] to(int x, int y) {
        int[] cell = new int[4];
        int[][] xy = new int[102][2];

        for (int i = 0;i < 4;i++) {
            /*
            * 0 - top
            * 1 - left
            * 2 - right
            * 3 - bottom
            */
            int c = 0;
            switch (i) {
                case 0:
                    c = getN(x,y+1);
                    if (c != -1) {
                        xy[c][0] = x;
                        xy[c][1] = y+1;
                    }
                    break;
                case 1:
                    c = getN(x-1,y);
                    if (c != -1) {
                        xy[c][0] = x-1;
                        xy[c][1] = y;
                    }
                    break;
                case 2:
                    c = getN(x+1,y);
                    if (c != -1) {
                        xy[c][0] = x+1;
                        xy[c][1] = y;
                    }
                    break;
                case 3:
                    c = getN(x,y-1);
                    if (c != -1) {
                        xy[c][0] = x;
                        xy[c][1] = y-1;
                    }
                    break;
                default:
                    System.err.println("Error!");
                    System.exit(1);
                    break;
            }
            switch (c) {
                case -1:
                    cell[i] = 101;
                    break;
                default:
                    cell[i] = c;
            }
        }
        Arrays.sort(cell);
        System.out.println(cell[0] + " " + cell[1] + " " + cell[2] + " " + cell[3] + " ");
        return xy[cell[0]];
    }

    private int getN(int x, int y) {
        if(x < 0 || y < 0) return 101;
        return map[y][x];
    }

    public static void main(String[] args) {
        new Main();
    }

    /*1 2
3 6
5 6
-1  0 -1 -1 -1 -1
-1  3 -1  0  3 -1
-1  2  5  2  4  0
-1  4  1  4  5 -1
-1 -1 -1 -1 -1 -1*/
}
